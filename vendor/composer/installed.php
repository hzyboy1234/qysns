<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'laravel/laravel',
  ),
  'versions' => 
  array (
    'bacon/bacon-qr-code' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '448ee9929aece0e86f0e2b926e636f9b53d03ce1',
    ),
    'box/spout' => 
    array (
      'pretty_version' => 'v3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9bdb027d312b732515b884a341c0ad70372c6295',
    ),
    'brick/math' => 
    array (
      'pretty_version' => '0.11.0',
      'version' => '0.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0ad82ce168c82ba30d1c01ec86116ab52f589478',
    ),
    'composer/semver' => 
    array (
      'pretty_version' => '3.4.0',
      'version' => '3.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '35e8d0af4486141bc745f23a29cc2091eb624a32',
    ),
    'cordoval/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'davedevelopment/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'dcat/easy-excel' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '20ee838b07f1f5d9c075b84e6f4807cbb21c44b0',
    ),
    'dflydev/dot-access-data' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f41715465d65213d644d3141a6a93081be5d3549',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1ca8f21980e770095a31456042471a57bc4c68fb',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => '3.7.0',
      'version' => '3.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '00d03067f07482f025d41ab55e4ba0db5eca2cdf',
    ),
    'doctrine/deprecations' => 
    array (
      'pretty_version' => 'v1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '612a3ee5ab0d5dd97b7cf3874a6efe24325efac3',
    ),
    'doctrine/event-manager' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '95aa4cb529f1e96576f3fda9f5705ada4056a520',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.8',
      'version' => '2.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f9301a5b2fb1216b2b08f02ba04dc45423db6bff',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0a0fa9780f5d4e507415a065172d26a98d02047b',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '39ab8fcf5a51ce4b85ca97c7a7d033eb12831124',
    ),
    'dragonmantank/cron-expression' => 
    array (
      'pretty_version' => 'v3.3.1',
      'version' => '3.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be85b3f05b46c39bbc0d95f6c071ddff669510fa',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '3.2.6',
      'version' => '3.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e5997fa97e8790cdae03a9cbd5e78e45e3c7bda7',
    ),
    'elastic/transport' => 
    array (
      'pretty_version' => 'v8.7.0',
      'version' => '8.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d7937f026393186f48b2e4fba6d8db85ca0dba6',
    ),
    'elasticsearch/elasticsearch' => 
    array (
      'pretty_version' => 'v8.8.1',
      'version' => '8.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7f4ccfe9b11c92191bc06119848efa115a22924c',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.16.0',
      'version' => '4.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '523407fb06eb9e5f3d59889b3978d5bfe94299c8',
    ),
    'fakerphp/faker' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e3daa170d00fde61ea7719ef47bb09bb8f1d9b01',
    ),
    'fideloper/proxy' => 
    array (
      'pretty_version' => '4.4.2',
      'version' => '4.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a751f2bc86dd8e6cfef12dc0cbdada82f5a18750',
    ),
    'filp/whoops' => 
    array (
      'pretty_version' => '2.15.3',
      'version' => '2.15.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c83e88a30524f9360b11f585f71e6b17313b7187',
    ),
    'firebase/php-jwt' => 
    array (
      'pretty_version' => 'v6.8.1',
      'version' => '6.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5dbc8959427416b8ee09a100d7a8588c00fb2e26',
    ),
    'fruitcake/php-cors' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '58571acbaa5f9f462c9c77e911700ac66f446d4e',
    ),
    'graham-campbell/result-type' => 
    array (
      'pretty_version' => 'v1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '672eff8cf1d6fe1ef09ca0f89c4b287d6a3eb831',
    ),
    'guzzlehttp/command' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3372bcfd79d4b357b6871665bf06155515e8d844',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '7.8.0',
      'version' => '7.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1110f66a6530a40fe7aea0378fe608ee2b2248f9',
    ),
    'guzzlehttp/guzzle-services' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f4bb1c205152a56741624b88753732e01a60565c',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '111166291a0f8130081195ac4556a5587d7f1b5d',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be45764272e8873c72dbe3d2edcfdfcc3bc9f727',
    ),
    'guzzlehttp/uri-template' => 
    array (
      'pretty_version' => 'v1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '61bf437fc2197f587f6857d3ff903a24f1731b5d',
    ),
    'hamcrest/hamcrest-php' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8c3d0a3f6af734494ad8f6fbbee0ba92422859f3',
    ),
    'handcraftedinthealps/elasticsearch-dsl' => 
    array (
      'pretty_version' => '8.0.2',
      'version' => '8.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '02c457f748ef2d046a338bf8c21a9a9b31327164',
    ),
    'illuminate/auth' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/broadcasting' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/bus' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/cache' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/collections' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/conditionable' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/config' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/console' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/container' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/cookie' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/database' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/encryption' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/events' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/filesystem' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/hashing' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/http' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/log' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/macroable' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/mail' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/notifications' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/pagination' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/pipeline' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/queue' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/redis' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/routing' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/session' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/support' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/testing' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/translation' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/validation' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/view' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'intervention/image' => 
    array (
      'pretty_version' => '2.7.2',
      'version' => '2.7.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '04be355f8d6734c826045d02a1079ad658322dad',
    ),
    'jetbrains/phpstorm-stubs' => 
    array (
      'pretty_version' => 'v2022.2',
      'version' => '2022.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '01006d9854679672fc8b85c6d5063ea6f25226ac',
    ),
    'kodova/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'laravel/framework' => 
    array (
      'pretty_version' => 'v9.2.0',
      'version' => '9.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '13372872bed31ae75df8709b9de5cde01d50646e',
    ),
    'laravel/laravel' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'laravel/sail' => 
    array (
      'pretty_version' => 'v1.24.1',
      'version' => '1.24.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3a373bb2845623aed2017c672dc61c84ae974890',
    ),
    'laravel/scout' => 
    array (
      'pretty_version' => 'v9.8.1',
      'version' => '9.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '38595717b396ce733d432b82e3225fa4e0d6c8ef',
    ),
    'laravel/serializable-closure' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e5a3057a5591e1cfe8183034b0203921abe2c902',
    ),
    'laravel/socialite' => 
    array (
      'pretty_version' => 'v5.9.1',
      'version' => '5.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '49ecc4c907ed88c1254bae991c6b2948945645c2',
    ),
    'laravel/tinker' => 
    array (
      'pretty_version' => 'v2.8.2',
      'version' => '2.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b936d415b252b499e8c3b1f795cd4fc20f57e1f3',
    ),
    'lcobucci/clock' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fb533e093fd61321bfcbac08b131ce805fe183d3',
    ),
    'lcobucci/jwt' => 
    array (
      'pretty_version' => '4.3.0',
      'version' => '4.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d7de2fe0d51a96418c0d04004986e410e87f6b4',
    ),
    'league/commonmark' => 
    array (
      'pretty_version' => '2.4.1',
      'version' => '2.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3669d6d5f7a47a93c08ddff335e6d945481a1dd5',
    ),
    'league/config' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '754b3604fb2984c71f4af4a9cbe7b57f346ec1f3',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '3.16.0',
      'version' => '3.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4fdf372ca6b63c6e281b1c01a624349ccb757729',
    ),
    'league/flysystem-local' => 
    array (
      'pretty_version' => '3.16.0',
      'version' => '3.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ec7383f25642e6fd4bb0c9554fc2311245391781',
    ),
    'league/mime-type-detection' => 
    array (
      'pretty_version' => '1.13.0',
      'version' => '1.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a6dfb1194a2946fcdc1f38219445234f65b35c96',
    ),
    'league/oauth1-client' => 
    array (
      'pretty_version' => 'v1.10.1',
      'version' => '1.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd6365b901b5c287dd41f143033315e2f777e1167',
    ),
    'maennchen/zipstream-php' => 
    array (
      'pretty_version' => '2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3fa72e4c71a43f9e9118752a5c90e476a8dc9eb3',
    ),
    'markbaker/complex' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c56caa1cf5c766ad6d65b6344b807c1e8405b9',
    ),
    'markbaker/matrix' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '728434227fe21be27ff6d86621a1b13107a2562c',
    ),
    'matchish/laravel-scout-elasticsearch' => 
    array (
      'pretty_version' => 'v7.3.0',
      'version' => '7.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '97e55780c7dc36a9c6a15317b1449fc8e9f0f3ba',
    ),
    'mockery/mockery' => 
    array (
      'pretty_version' => '1.6.6',
      'version' => '1.6.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b8e0bb7d8c604046539c1115994632c74dcb361e',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '2.9.1',
      'version' => '2.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f259e2b15fb95494c83f52d3caad003bbf5ffaa1',
    ),
    'mtdowling/cron-expression' => 
    array (
      'replaced' => 
      array (
        0 => '^1.0',
      ),
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7284c22080590fb39f2ffa3e9057f10a4ddd0e0c',
    ),
    'myclabs/php-enum' => 
    array (
      'pretty_version' => '1.8.4',
      'version' => '1.8.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a867478eae49c9f59ece437ae7f9506bfaa27483',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.70.0',
      'version' => '2.70.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd3298b38ea8612e5f77d38d1a99438e42f70341d',
    ),
    'nette/schema' => 
    array (
      'pretty_version' => 'v1.2.4',
      'version' => '1.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c9ff517a53903b3d4e29ec547fb20feecb05b8ab',
    ),
    'nette/utils' => 
    array (
      'pretty_version' => 'v4.0.2',
      'version' => '4.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c67c9f33ab9b3026709c7e606d93f7044678f8df',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.17.1',
      'version' => '4.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a6303e50c90c355c7eeee2c4a8b27fe8dc8fef1d',
    ),
    'nunomaduro/collision' => 
    array (
      'pretty_version' => 'v6.4.0',
      'version' => '6.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f05978827b9343cba381ca05b8c7deee346b6015',
    ),
    'ongr/elasticsearch-dsl' => 
    array (
      'replaced' => 
      array (
        0 => '8.0.2',
      ),
    ),
    'overtrue/easy-sms' => 
    array (
      'pretty_version' => '2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '81d4deec69bbb6de6e5fdd7ab90cc933bd3e3046',
    ),
    'overtrue/pinyin' => 
    array (
      'pretty_version' => '5.2.2',
      'version' => '5.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1cb6fdfe0c90d1895bb2f877a33ab2c0b58cb21c',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '97803eca37d319dfa7826cc2437fc020857acb53',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '3.2.1',
      'version' => '3.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4f7fd7836c6f332bb2933569e566a0d6c4cbed74',
    ),
    'php-http/async-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '*',
      ),
    ),
    'php-http/client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '*',
      ),
    ),
    'php-http/discovery' => 
    array (
      'pretty_version' => '1.19.1',
      'version' => '1.19.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '57f3de01d32085fea20865f9b16fb0e69347c39e',
    ),
    'php-http/httplug' => 
    array (
      'pretty_version' => '2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '625ad742c360c8ac580fcc647a1541d29e257f67',
    ),
    'php-http/promise' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4c4c1f9b7289a2ec57cde7f1e9762a5789506f88',
    ),
    'phpoffice/phpspreadsheet' => 
    array (
      'pretty_version' => '1.29.0',
      'version' => '1.29.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fde2ccf55eaef7e86021ff1acce26479160a0fa0',
    ),
    'phpoption/phpoption' => 
    array (
      'pretty_version' => '1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dd3a383e599f49777d8b628dadbb90cae435b87e',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '9.2.28',
      'version' => '9.2.28.0',
      'aliases' => 
      array (
      ),
      'reference' => '7134a5ccaaf0f1c92a4f5501a6c9f98ac4dcc0ef',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '3.0.6',
      'version' => '3.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cf1c2e7c203ac650e352f4cc675a7021e7d1b3cf',
    ),
    'phpunit/php-invoker' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a10147d0aaf65b58940a0b72f71c9ac0423cc67',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '5da5f67fc95621df9ff4c4e5a84d6a8a2acf7c28',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '5.0.3',
      'version' => '5.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a63ce20ed1b5bf577850e2c4e87f4aa902afbd2',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '9.6.13',
      'version' => '9.6.13.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3d767f7f9e191eab4189abe41ab37797e30b1be',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aa5030cfa5405eccfdcb1083ce040c2cb8d253bf',
    ),
    'psr/clock' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e41a24703d4560fd0acb709162f73b8adfc3aa0d',
    ),
    'psr/clock-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c71ecc56dfe541dbd90c5360474fbc405f8d5963',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1|2.0',
      ),
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb5906edc1c324c9a05aa0873d40117941e5fa90',
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
        1 => '*',
      ),
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e616d01114759c4c489f93b099585439f795fe35',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
        1 => '*',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.1',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb6ce4845ce34a8ad9e68117c10ee90a29919eba',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
        1 => '*',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fe5ea303b0887d5caefd3d431c3e61ad47037001',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0 || 2.0.0 || 3.0.0',
        1 => '1.0|2.0|3.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '764e0b3939f5ca87cb904f570ef9be2d78a07865',
    ),
    'psr/simple-cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0|3.0',
      ),
    ),
    'psy/psysh' => 
    array (
      'pretty_version' => 'v0.11.21',
      'version' => '0.11.21.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bcb22101107f3bf770523b65630c9d547f60c540',
    ),
    'qcloud/cos-sdk-v5' => 
    array (
      'pretty_version' => 'v2.6.6',
      'version' => '2.6.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d82ccb550fe2dca1adfb53835791d314023a9a8',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'ramsey/collection' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ad7475d1c9e70b190ecffc58f2d989416af339b4',
    ),
    'ramsey/uuid' => 
    array (
      'pretty_version' => '4.7.4',
      'version' => '4.7.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '60a4c63ab724854332900504274f6150ff26d286',
    ),
    'rayswoole/querylist' => 
    array (
      'pretty_version' => '1.0.4',
      'version' => '1.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '95bc3bc9f8fcd620f325c711ff1a85a6841ecffe',
    ),
    'rhumsaa/uuid' => 
    array (
      'replaced' => 
      array (
        0 => '4.7.4',
      ),
    ),
    'roave/better-reflection' => 
    array (
      'pretty_version' => '5.11.1',
      'version' => '5.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '84cdf580747c3d55238be28d5681e24e7d11e065',
    ),
    'roave/signature' => 
    array (
      'pretty_version' => '1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2ab4eadcb9f9d449f673a97b67797403b35eca94',
    ),
    'sebastian/cli-parser' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '442e7c7e687e42adc03470c7b668bc4b2402c0b2',
    ),
    'sebastian/code-unit' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '1fc9f64c0927627ef78ba436c9b17d967e68e120',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ac91f01ccec49fb77bdc6fd1e548bc70f7faa3e5',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '4.0.8',
      'version' => '4.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa0f136dd2334583309d32b62544682ee972b51a',
    ),
    'sebastian/complexity' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '739b35e53379900cc9ac327b2147867b8b6efd88',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '4.0.5',
      'version' => '4.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '74be17022044ebaaecfdf0c5cd504fc9cd5a7131',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '5.1.5',
      'version' => '5.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '830c43a844f1f8d5b7a1f6d6076b784454d8b7ed',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '4.0.5',
      'version' => '4.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ac230ed27f0f98f597c8a2b6eb7ac563af5e5b9d',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '5.0.6',
      'version' => '5.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bde739e7565280bda77be70044ac1047bc007e34',
    ),
    'sebastian/lines-of-code' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1c2e997aa3146983ed888ad08b15470a2e22ecc',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '5c9eeac41b290a3712d88851518825ad78f45c71',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b4f479ebdbf63ac605d183ece17d8d7fe49c15c7',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '4.0.5',
      'version' => '4.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e75bd0f07204fec2a0af9b0f3cfe97d05f92efc1',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f4443cb3a1d92ce809899753bc0d5d5a8dd19a8',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => '3.2.1',
      'version' => '3.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '75e2c2a32f5e0b3aef905b9ed0b179b953b3d7c7',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6c1022351a901512170118436c764e473f6de8c',
    ),
    'simplesoftwareio/simple-qrcode' => 
    array (
      'pretty_version' => '1.3.3',
      'version' => '1.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '17c5e45c79c40f717d4bc08cf5e568f29ebf9333',
    ),
    'socialiteproviders/apple' => 
    array (
      'pretty_version' => '5.6.0',
      'version' => '5.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4f0f06e463824f0df6151c768db2fec6610ea055',
    ),
    'socialiteproviders/manager' => 
    array (
      'pretty_version' => 'v4.4.0',
      'version' => '4.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'df5e45b53d918ec3d689f014d98a6c838b98ed96',
    ),
    'socialiteproviders/qq' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a588df598c81dbf917f8f99d6d685212774c5b1b',
    ),
    'socialiteproviders/wechat-service-account' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '56d1d4bb3972b2b30da9db2c0a2bda9b01cd3f46',
    ),
    'socialiteproviders/wechat-web' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0cc9d25c80ee51eba5cb7dfe4b4df8f7822d6c09',
    ),
    'socialiteproviders/weixin' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4f5ca3ee25e1a46e0dc6bb00bd6ba77f344e340f',
    ),
    'socialiteproviders/weixin-web' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3862d3a2e3e55bb0381efd0b73958bfc4dffd7b3',
    ),
    'spatie/backtrace' => 
    array (
      'pretty_version' => '1.5.3',
      'version' => '1.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '483f76a82964a0431aa836b6ed0edde0c248e3ab',
    ),
    'spatie/eloquent-sortable' => 
    array (
      'pretty_version' => '4.0.1',
      'version' => '4.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '64a3365c0d5a7b4a1837b2f29d01ee4c578c416a',
    ),
    'spatie/flare-client-php' => 
    array (
      'pretty_version' => '1.4.2',
      'version' => '1.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f2c6a7a0d2c1d90c12559dc7828fd942911a544',
    ),
    'spatie/ignition' => 
    array (
      'pretty_version' => '1.11.2',
      'version' => '1.11.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '48b23411ca4bfbc75c75dfc638b6b36159c375aa',
    ),
    'spatie/laravel-ignition' => 
    array (
      'pretty_version' => '1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '29deea5d9cf921590184be6956e657c4f4566440',
    ),
    'spatie/laravel-package-tools' => 
    array (
      'pretty_version' => '1.12.1',
      'version' => '1.12.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '09f80fa240d44fafb1c70657c74ee44ffa929357',
    ),
    'stella-maris/clock' => 
    array (
      'pretty_version' => '0.1.7',
      'version' => '0.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa23ce16019289a18bb3446fdecd45befcdd94f8',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c3ebc83d031b71c39da318ca8b7a07ecc67507ed',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f1d00bddb83a4cb2138564b2150001cb6ce272b1',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '26954b3d62a6c5fd0ea8a2a00c0353a14978d05c',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c7df52182f43a68522756ac31a532dd5b1e6db67',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '2eaf8e63bc5b8cefabd4a800157f0d0c094f677a',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '7bc61cc2db649b4637d331240c5346dcc7708051',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0|3.0',
      ),
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '5cc9cac6586fc0c28cd173780ca696e419fefa11',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v6.0.20',
      'version' => '6.0.20.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e16b2676a4b3b1fa12378a20b29c364feda2a8d6',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v6.0.20',
      'version' => '6.0.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '6dc70833fd0ef5e861e17c7854c12d7d86679349',
    ),
    'symfony/mailer' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd60799210c488f545ddde2444dc1aa548322872',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd7052547a0070cbeadd474e172b527a00d657301',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ea208ce43cbb04af6867b4fdddb1bdbf84cc28cb',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '875e90aeea2777b6f135677f618529449334a612',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ecaafce9f77234a6a449d29e49267ba10499116d',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8c4ad05dd0120b6a53c1ca374dca2ad0a1c4ed92',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '42292d99c55abe617799667f454222c54c60e229',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '70f4aebd92afca2f865444d30a4d2151c13c3179',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6caa57379c4aec19c0a12a38b59b26487dcfe4b5',
    ),
    'symfony/polyfill-php81' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7581cd600fa9fd681b797d00b02f068e2f13263b',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '2114fd60f26a296cc403a7939ab91478475a33d4',
    ),
    'symfony/psr-http-message-bridge' => 
    array (
      'pretty_version' => 'v2.3.1',
      'version' => '2.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '581ca6067eb62640de5ff08ee1ba6850a0ee472e',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e56ca9b41c1ec447193474cd86ad7c0b547755ac',
    ),
    'symfony/serializer' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '6aff9e2894ff0dcfa7aca38f9d0396ed65627d07',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd78d39c1599bd1188b8e26bb341da52c3c6d8a66',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd9e72497367c23e08bf94176d2be45b00a9d232a',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '9c24b3fdbbe9fb2ef3a6afd8bbaadfd72dad681f',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'acbfbb274e730e5a0236f619b6168d9dedb3e282',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3|3.0',
      ),
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eb980457fa6899840fe1687e8627a03a7d8a3d52',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'deec3a812a0305a50db8ae689b183f43d915c884',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '34a41e998c2183e22995f158c581e7b5e755ab9e',
    ),
    'tijsverkoyen/css-to-inline-styles' => 
    array (
      'pretty_version' => '2.2.6',
      'version' => '2.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c42125b83a4fa63b187fdf29f9c93cb7733da30c',
    ),
    'topthink/think-helper' => 
    array (
      'pretty_version' => 'v3.1.6',
      'version' => '3.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '769acbe50a4274327162f9c68ec2e89a38eb2aff',
    ),
    'vlucas/phpdotenv' => 
    array (
      'pretty_version' => 'v5.5.0',
      'version' => '5.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1a7ea2afc49c3ee6d87061f5a233e3a035d0eae7',
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b56450eed252f6801410d810c8e1727224ae0743',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '11cb2199493b2f8a3b53e7f19068fc6aac760991',
    ),
    'yansongda/supports' => 
    array (
      'pretty_version' => 'v3.2.5',
      'version' => '3.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c3f736efe169696cef94730976e604a61c345b5c',
    ),
  ),
);
