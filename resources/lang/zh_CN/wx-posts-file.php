<?php
return [
    'labels' => [
        'WxPostsFile' => 'WxPostsFile',
        'wx-posts-file' => 'WxPostsFile',
    ],
    'fields' => [
        'name' => '文件名称',
        'size' => '文件大小',
        'url' => '文件地址',
        'post_id' => '帖子id',
    ],
    'options' => [
    ],
];
