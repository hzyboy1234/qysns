<?php 
return [
    'labels' => [
        'WxPostsPayContent' => '付费内容',
        'wx-posts-pay-content' => '付费内容',
    ],
    'fields' => [
        'post_id' => '帖子id',
        'words_percent' => '字数百分比',
        'is_file' => '附件付费',
        'is_sound' => '音频付费',
        'hidden' => '隐藏内容',
        'price' => '价格',
        'credit_type' => '0：金币 1：余额',
    ],
    'options' => [
    ],
];
