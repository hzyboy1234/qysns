<?php 
return [
    'labels' => [
        'WxUserAvatarFramePurchase' => '头像框购买记录',
        'wx-user-avatar-frame-purchase' => '头像框购买记录',
    ],
    'fields' => [
        'user_id' => '用户id',
        'avatar_frame_id' => '头像框id',
        'purchase_way' => '解锁方式',
        'credit_type' => '0：金币 1：余额',
        'price' => '解锁时价格',
        'activity_id' => '解锁活动id',
        'description' => '说明',
    ],
    'options' => [
    ],
];
