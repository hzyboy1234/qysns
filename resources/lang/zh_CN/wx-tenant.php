<?php
return [
    'labels' => [
        'WxTenant' => '分站表',
        'wx-tenant' => '分站表',
    ],
    'fields' => [
        'tenant_id' => '分站ID',
        'name' => '名称',
        'short' => '简称',
        'logo' => 'Logo',
        'desc' => '描述',
        'status' => '状态',
        'tip' => '备注'
    ],
    'options' => [
    ],
];
