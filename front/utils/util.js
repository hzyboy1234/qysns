// 防抖
let t = null
export const debounce = function (fn, delay) {
  if (t !== null) {
    clearTimeout(t)
  }
  t = setTimeout(() => {
    fn()
  }, delay)
}
/**
 * 获取用户地址
 */
const checkPermission = function (t, e, n) {
	return new Promise(function (resolve, reject) {
		uni.getSetting({
			success: function (a) {
				null == a.authSetting[t] ?
					uni.authorize({
						scope: t,
						success: () => n(),
						fail: (err) => {
							console.log(err)
						}
					}) :
					a.authSetting[t] ?
						n() :
						uni.showModal({
							title: '提示',
							content: e,
							success: (t) => {
								t.confirm ? uni.openSetting({}) : t.cancel;
							}
						});
				resolve({})
			},
			fail: function (e) {
				console.log(e);
				reject({})
			}
		});
	});
};

/**
 * 微信的的request
 */

function request(url, data = {}, method = 'GET') {
  if (method == 'POST') {
    if (uni.$store.state.posting_url == url) {
      uni.miniLoading(false)
      uni.showToast({
        title: '别急，慢一点',
        icon: 'none'
      })
      return false
    } else {
      uni.$store.commit('updatePostingUrl', url)
    }
  }
  return new Promise(function (resolve, reject) {
    uni.request({
      url,
      data,
      method,
      header: {
        'Content-Type': 'application/json',
        token: uni.getStorageSync('token'),
        played: uni.$store.state.played,
        fromUser: uni.$store.state.form_user,
        tenant: uni.$store.state.tenant.tenant_id,
        safeCode: uni.wen.api.safeCode,
        platform: uni.$store.state.platform,
		scene: uni.$store.state.scene,
        // #ifdef MP
        device: 'mp',
        // #endif
        // #ifdef H5
        device: 'h5',
        // #endif
        // #ifdef APP
        device: 'app',
        // #endif
      },
      success: function (res) {
        // console.log('===============请求数据==================');
        // console.log("url",url)
        // console.log("请求data：", JSON.stringify(data))
        // console.log("res",res)
        // console.log('-----------------------------------------');
        if (method == 'POST') {
          uni.$store.commit('updatePostingUrl', '')
        }
        uni.miniLoading(false)
        if (res.statusCode == 200) {
          if (res.data.code == 503002) {
            if (uni.$store.state.scene == 14) {
              return
            }
            uni.wen.toUrl(-4, null, null)
          } else if (res.data.code == 503003) {
            //token过期
            // #ifdef MP
            getNewToken().
            then(() => {
              request(url, data, method).
              then((res) => {
                resolve(res)
              })
            })
            // #endif

            // #ifndef MP
            //非小程序 ，token过期的话，就直接跳登录
            console.log('token过期 跳登录11')
            uni.removeStorageSync('token')
            uni.$store.commit('updateToken', '')

            uni.removeStorageSync('userInfo')

            debounce(function () {
              console.log('token过期 跳登录22')
              if (uni.$store.state.scene == 14) {
                return
              }
              uni.wen.toUrl(-4, null, null)
            }, 1000)
            // #endif
          } else {
            resolve(res.data)
          }
        } else {
          reject(res.errMsg)
        }
      },
      fail: function (err) {
        if (method == 'POST') {
          uni.$store.commit('updatePostingUrl', '')
        }
        console.log('request err', err, url)
        reject(err)
      }
    })
  })
}

/**
 * 微信的上传uploadFile
 */

function uploadFile(url, filePath, scene) {
  let that = this
  return new Promise(function (resolve, reject) {
    uni.uploadFile({
      url: url,
      filePath: filePath,
      name: 'file',
      header: {
        token: uni.getStorageSync('token'),
        played: uni.$store.state.played,
        safeCode: uni.wen.api.safeCode,
        tenant: uni.$store.state.tenant.id,
        // #ifdef MP
        device: 'mp',
        // #endif
        // #ifdef H5
        device: 'h5',
        // #endif
        // #ifdef APP
        device: 'app',
        // #endif
        scene: scene
      },
      success: function (res) {
        uni.miniLoading(false)
        let data = JSON.parse(res.data)
        if (res.statusCode == 200) {
          if (data.code == 503002) {
            if (uni.$store.state.scene == 14) {
              return
            }
            uni.wen.toUrl(-4, null, null)
          } else if (data.code == 503003) {
            //token过期
            // #ifdef MP
            getNewToken().
            then(() => {
              uploadFile(url, filePath, scene).
              then((res) => {
                resolve(res)
              })
            })
            // #endif

            // #ifndef MP
            //非小程序 ，token过期的话，就直接跳登录
            console.log('token过期 跳登录11')
            uni.removeStorageSync('token')
            uni.$store.commit('updateToken', '')

            uni.removeStorageSync('userInfo')
            that.$store.commit('updateUserInfo',
              {
                id: '',
                user_name: '未登录用户',
                user_avatar: that.$store.state.config.img.default_avatar,
                user_introduce: that.$store.state.config.user.user_default_introduce,
                user_background_maps: that.$store.state.config.user.user_background_maps,
                is_authentication: false,
                is_member: false,
                is_official: false,
                is_played: false,
                gender: 0
              },
            )

            debounce(function () {
              console.log('token过期 跳登录22')
              if (uni.$store.state.scene == 14) {
                return
              }
              uni.wen.toUrl(-4, null, null)
            }, 1000)
            // #endif
          } else {
            resolve(data)
          }
        } else {
          reject(res.errMsg)
        }
      },
      fail: function (err) {
        reject(err)
      }
    })
  })
}

/**
 * 更新token
 */
function getNewToken() {
  let code = null
  console.log('getNewToken', 1)
  return new Promise((resolve, reject) => {
    // #ifdef MP
    return login().
    then((res) => {
      code = res.code
      return getUserInfo()
    }).
    then((userInfo) => {
      //登录远程服务器
      request(
        uni.wen.api.loginUrl, {
          code: code,
          encryptedData: userInfo.encryptedData,
          iv: userInfo.iv
        },
        'POST'
      ).
      then((res) => {
        if (res.code === 200) {
          uni.setStorageSync('token', res.data.token)
          uni.$store.commit('updateToken', res.data.token)

          let device = ''
          // #ifdef APP
          device = 'app'
          // #endif
          // #ifdef MP
          device = 'mp'
          // #endif
          // #ifdef H5
          device = 'h5'
          // #endif
          uni.sendSocketMessage({
            data: '{"mode":"bind","token":"' + uni.$store.state.token + '", "played": "' + uni.$store.state.played + '", "device": "' + device + '"}'
          })
          resolve(res)
        } else {
          reject(res)
        }
      }).
      catch((err) => {
        reject(err)
      })
    }).
    catch((err) => {
      reject(err)
    })
    // #endif

    // #ifndef Mp
    resolve()
    // #endif
  })
}

/**
 * 检查微信会话是否过期
 */

function checkSession() {
  return new Promise(function (resolve, reject) {
    uni.checkSession({
      success: function () {
        resolve(true)
      },
      fail: function () {
        reject(false)
      }
    })
  })
}

/**
 * 调用微信登录
 */
function login() {
  return new Promise(function (resolve, reject) {
    uni.login({
      success: function (res) {
        if (res.code) {
          //登录远程服务器
          resolve(res)
        } else {
          reject(res)
        }
      },
      fail: function (err) {
        console.log(err)
        reject(err)
      }
    })
  })
}

function getUserInfo() {
  return new Promise(function (resolve, reject) {
    uni.getUserInfo({
      withCredentials: true,
      success: function (res) {
        resolve(res)
      },
      fail: function (err) {
        reject(err)
      }
    })
  })
}

//更新用户信息
function updateUserInfo() {
  setTimeout(function () {
    uni.wen.util.request(uni.wen.api.userInfoUrl, {
		cid: uni.$store.state.cid
	}).
    then(function (res) {
      if (res.status) {
        uni.setStorageSync('userInfo', res.data);
        uni.$store.commit('updateUserInfo', res.data);
        uni.$store.commit('userUpdateTimes', 1);

        if (res.code == 200025) {
          uni.showModal({
            title: '用户名为空',
            content: '系统检测到您的用户名为空，是否前往修改？',
            confirmText: '立即前往',
            cancelText: '下次',
            success: function (res) {
              if (res.confirm) {
                uni.wen.toUrl(6, '/pagesA/mine/editmine/editmine')
              } else if (res.cancel) {

              }
            }
          })
        }
      }
    })
  }, 500)

}

/**
 * 判断页面是否需要登录
 */

function loginNow() {
  let that = this
  let token = uni.$store.state.token

  if (token == '' || token === undefined) {
    if (uni.$store.state.scene == 14) {
      return
    }
    uni.wen.toUrl(-4, null, null)
    return false
  } else {
    return true
  }
}

function log(data) {
  request(uni.wen.api.putLogUrl, { log: data }, 'POST').
  then(function (res) {
    if (res.status) {

    } else {
      uni.showToast({
        title: res.message,
        icon: 'none',
        duration: 1500
      })
    }
  })
}

/**
 *
 * @param {*} source 源数组
 * @param {*} count 要取出多少项
 * @param {*} isPermutation 是否使用排列的方式
 * @return {any[]} 所有排列组合,格式为 [ [1,2], [1,3]] ...
 */

function getNumbers(source, count, isPermutation = true) {
  //如果只取一位，返回数组中的所有项，例如 [ [1], [2], [3] ]
  let currentList = source.map((item) => [item])

  if (count === 1) {
    return currentList
  }

  let result = [] //取出第一项后，再取出后面count - 1 项的排列组合，并把第一项的所有可能（currentList）和 后面count-1项所有可能交叉组合

  for (let i = 0; i < currentList.length; i++) {
    let current = currentList[i] //如果是排列的方式，在取count-1时，源数组中排除当前项

    let children = []

    if (isPermutation) {
      children = getNumbers(
        source.filter((item) => item !== current[0]),
        count - 1,
        isPermutation
      )
    } //如果是组合的方法，在取count-1时，源数组只使用当前项之后的
    else {
      children = getNumbers(source.slice(i + 1), count - 1, isPermutation)
    }

    for (let child of children) {
      result.push(current + '_' + child)
      result.push(child + '_' + current)
    }
  }

  return result
}

function setClipboardData(str, toast) {
  if (!str) {
    uni.showToast({
      title: '复制出错',
      icon: 'none',
      mask: 'true'
    })
  }

  uni.setClipboardData({
    data: str.toString(),
    success(res) {
      if (toast) {
        uni.showToast({
          title: toast.replace('[data]', res.data),
          icon: 'none',
          mask: 'true'
        })
      } else {
        uni.showToast({
          title: '[' + res.data + ']复制成功',
          icon: 'none',
          mask: 'true'
        })
      }
    },

    fail(res) {
      console.log('复制出错' + JSON.stringify(res))
    }
  })
}

function htmlEncodeByRegExp(str) {
  var s = ''

  if (str.length === 0) {
    return ''
  }

  s = str.replace(/&/g, '&amp;')
  s = s.replace(/</g, '&lt;')
  s = s.replace(/>/g, '&gt;')
  s = s.replace(/ /g, '&nbsp;')
  s = s.replace(/\'/g, '&#39;')
  s = s.replace(/\"/g, '&quot;')
  return s
} // 4.使用正则实现html解码

function htmlDecodeByRegExp(str) {
  var s = ''

  if (str.length === 0) {
    return ''
  }

  s = str.replace(/&amp;/g, '&')
  s = s.replace(/&lt;/g, '<')
  s = s.replace(/&gt;/g, '>')
  s = s.replace(/&nbsp;/g, ' ')
  s = s.replace(/&#39;/g, '\'')
  s = s.replace(/&quot;/g, '"')
  return s
}

// 去掉所有的图片样式后缀
function removeStyles(url) {
  let styleNames = uni.$store.state.config.img.styleNames || new Array()
  if (url && styleNames) {
    for (var i = 0; i < styleNames.length; i++) {
      if (styleNames[i].charAt(0) == '/') {
        url = url.replace(styleNames[i], '')
      }
    }
  }
  return url
}

// 唤起指定qq或者qq群聊， todo：2=群聊
function awakenQQ(qq, type = 1) {
  if (type == 1) {
    // #ifdef APP
    plus.runtime.openURL('mqq://im/chat?chat_type=wpa&uin=' + qq + '&version=1&src_type=web ')
    // #endif
    // #ifdef H5
    window.open('mqq://im/chat?chat_type=wpa&uin=' + qq + '&version=1&src_type=web ')
    // #endif
  } else {
    // 将QQ群的号码转换成秘钥
    console.log('唤醒QQ群聊' + qq)
  }
}

function textRaw(text, length = 0) {
  if (text) {
    text = text.replace(/<[^>]+>/g, '').
    replace(/&nbsp;/ig, '')
    if (length > 0) {
      if (text.length >= length) {
        text = text.substring(0, length) + '...'
      }
    }
  }

  return text
}

//扫码
function scanCodeMethods() {
  //扫码
  uni.scanCode({
    onlyFromCamera: true,

    success(res) {
      console.log('scanCode', res)
      if (typeof res.path != 'undefined') {
        uni.wen.toUrl(6, '/' + res.path, null)
      }
    },
    fail(err) {
      console.log(err)
    }
  })
}

function parseUrlQuery(path) {
  let query = {}
  let queryList = path.split('?')
  //获取query
  if (queryList[1]) {
    let queryDataList = queryList[1].split('&')
    queryDataList.map((item) => {
      let queryData = item.split('=')
      query[queryData[0]] = queryData[1]
    })
    return query
  } else {
    return query
  }
}

/**
 *打开微信小程序
 *  {appId:小程序appid,gid:微信小程序原始id}*/
function toMpWeixin(appid, gid = '', path = '') {
  // #ifdef MP-WEIXIN
  uni.navigateToMiniProgram({ appId: appid, path: path })
  // #endif

  // #ifdef APP-PLUS
  plus.share.getServices(shareList => {
    let shareWx = shareList.find(val => val.id == 'weixin')
    if (shareWx) {
      shareWx.launchMiniProgram({ id: gid, path: path })
    } else {
      uni.showToast({ icon: 'none', title: '未安装微信,无法打开对应小程序' })
    }
  }, e => {
    uni.showToast({ icon: 'none', title: '获取分享服务列表失败:' + JSON.stringify(e) })
  })
  // #endif
}

function pointGridCode(x, y) {
  const columnWidth = uni.$store.state.screenWidth / 3
  const rowHeight = uni.$store.state.screenLiHeight / 4
  const columnIndex = Math.floor(x / columnWidth)
  const rowIndex = Math.floor(y / rowHeight)
  // 判断列位置
  switch (columnIndex) {
    case 0:
      switch (rowIndex) {
        case 0:
          return 1
        case 1:
          return 4
        case 2:
          return 7
        case 3:
          return 10
      }
      break
    case 1:
      switch (rowIndex) {
        case 0:
          return 2
        case 1:
          return 5
        case 2:
          return 8
        case 3:
          return 11
      }
      break
    case 2:
      switch (rowIndex) {
        case 0:
          return 3
        case 1:
          return 6
        case 2:
          return 9
        case 3:
          return 12
      }
      break
  }
  return 0
}

function deepClone(obj) {
  if (obj) {
    let objClone = Array.isArray(obj) ? [] : {}
    if (obj && typeof obj === 'object') {
      for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
          //判断ojb子元素是否为对象，如果是，递归复制
          if (obj[key] && typeof obj[key] === 'object') {
            objClone[key] = deepClone(obj[key])
          } else {
            //如果不是，简单复制
            objClone[key] = obj[key]
          }
        }
      }
    }
    return objClone
  }
  return null
}

function validatePhoneNumber(phoneNumber) {
  if (phoneNumber === undefined || phoneNumber === null || phoneNumber === '') {
    return false
  }
  const pattern = /^1[0-9]{10}$/
  return pattern.test(phoneNumber)
}

// 检查基础库是否支持
function hostVersionSupport(version) {

  let v1_version = uni.$store.state.SDKVersion

  let v1 = v1_version.split('.')
  let v2 = version.split('.')
  const len = Math.max(v1.length, v2.length)

  while (v1.length < len) {
    v1.push('0')
  }
  while (v2.length < len) {
    v2.push('0')
  }

  for (let i = 0; i < len; i++) {
    const num1 = parseInt(v1[i])
    const num2 = parseInt(v2[i])

    if (num1 > num2) {
      return true
    } else if (num1 < num2) {
      return false
    }
  }

  return true
}

// App打开或下载
function openOrDownloadApp() {
  const config_app = uni.$store.state.config.app
  const fn = function () {
    if (uni.$store.state.platform == 'ios') {
      const loadDateTime = new Date()
      window.location = `${config_app.basic.url_schemes}://` //schema链接或者universal link
      window.setTimeout(() => { //如果没有安装app,便会执行setTimeout跳转下载页
        var timeOutDateTime = new Date()
        if (timeOutDateTime - loadDateTime < 5000) {
          uni.wen.toUrl(6, '/pagesC/redirect/redirect', null);
        } else {
          window.close()
        }
      }, 500)
    } else if (uni.$store.state.platform == 'android') {
      try {
        window.location = `${config_app.basic.url_schemes}://` //schema链接或者universal link
        window.setTimeout(function () {
          uni.wen.toUrl(6, '/pagesC/redirect/redirect', null);
        }, 500)
      } catch (e) {}
    }
  }

  if (uni.$store.state.h5_browser === 'wxclient') {
    uni.showModal({
      title: '温馨提示',
      content: '请点击右上角，点击在默认浏览器中打开～！',
      showCancel: false,
      success: result => fn()
    })
  } else {
    fn()
  }
}


function doVibrateShort() {
	// #ifdef APP-PLUS
	if (uni.$store.state.platform == 'ios') {
		const UISelectionFeedbackGenerator = plus.ios.importClass('UISelectionFeedbackGenerator');
		const feedbackGenerator = new UISelectionFeedbackGenerator();
		feedbackGenerator.init();
		setTimeout(() => {
			feedbackGenerator.selectionChanged();
		}, 0)
	} else {
		plus.device.vibrate(15);
	}
	// #endif
	// #ifndef APP-PLUS
	uni.vibrateShort();
	// #endif
};


function getLocation() {
    return new Promise(function (resolve, reject) {
		const _getLocation = () => {
			uni.getLocation({
				type: 'gcj02',
				altitude: true,
				success: (res) => {
					console.log(res)
					resolve(res)
				},
				fail: (err) => {
					console.log(err)
					reject(err)
				}
			});
		}

		// #ifdef MP
		checkPermission('scope.userLocation', '请到设置页面授权“位置信息”权限', () => _getLocation()).catch(err => {
			reject(err)
		});
		// #endif

		// #ifndef MP
		_getLocation()
		// #endif
	});
};


function getGeocoder() {
    return new Promise(function (resolve, reject) {
		let resData = null;
        getLocation().then(res => {
            uni.wen.util.request(uni.wen.api.geocodeUrl, {latitude: res.latitude, longitude: res.longitude}, 'POST').then(res => {
                if (res.status) {
					resData = res.data;
                    resolve(res.data)
                } else {
                    uni.showToast({title: res.message, icon: 'none', duration: 1500})
                    reject(null)
                }
            }).catch(err => {
                reject(err)
            }).finally(()=>{
				if(resData){
					resolve(resData)
				}else{
					reject({});
				}
			})
        }).catch(err => {
            // TODO 只是测试代码、微信优化协议后在处理
            if (err.errno === 104) {
                uni.showToast({title: '未同意微信隐私保护协议，重新登陆下吧~', icon: 'none', duration: 2000});
            } else {
                uni.showToast({title: '定位失败~', icon: 'none', duration: 2000});
            }
            reject(err)
        })
    });
};

module.exports = {
  checkPermission,
  request,
  uploadFile,
  getNewToken,
  checkSession,
  login,
  getUserInfo,
  loginNow,
  getNumbers,
  setClipboardData,
  htmlEncodeByRegExp,
  htmlDecodeByRegExp,
  awakenQQ,
  removeStyles,
  textRaw,
  scanCodeMethods,
  parseUrlQuery,
  toMpWeixin,
  updateUserInfo,
  pointGridCode,
  log,
  deepClone,
  validatePhoneNumber,
  hostVersionSupport,
  openOrDownloadApp,
  doVibrateShort,
  getLocation,
  getGeocoder
}
