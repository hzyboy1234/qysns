const domain = 'mini.chongyeapp.com'; // 配置这里的域名，不带https
const safeCode = ''; // 安全码（可选） 请看教程：https://doc.chongyeapp.com/doc/68/

const ApiRootUrl = 'https://' + domain + '/api/v1/'; //v1接口
const WebRootUrl = 'https://' + domain + '/';
module.exports = {
	debug: false,
	safeCode: safeCode,
	homeUrl: 'https://' + domain,
	ApiRootUrl: ApiRootUrl,
	SocketUrl: 'wss://' + domain + '/wss',
    // 用户
	// 登录
    loginUrl: ApiRootUrl + 'login',
	// 绑定手机号码
	bindPhoneUrl: ApiRootUrl + 'user/bind/phone',
	// 获取验证码
	loginPhoneCodeUrl: ApiRootUrl + 'login/phone/code',
	// 验证码登录
	loginPhoneUrl: ApiRootUrl + 'login/phone',

    appLoginUrl: ApiRootUrl + 'app/login',

	webviewUrl: WebRootUrl + 'common/webview',

	// app版本更新
	appUpdateUrl: ApiRootUrl + 'app/update',

	// app引导图
	appGuideUrl: ApiRootUrl + 'app/guide',

	//登录接口
	// OAuth2.0 授权登录系统返回对应code的用户信息
    appLoginCodeUrl: ApiRootUrl + 'app/login/code',

	appLoginAppleUrl: ApiRootUrl + 'app/login/apple',
	appLoginQQUrl: ApiRootUrl + 'app/login/qq',
	// 用户注销账号
	userLogoffUrl: ApiRootUrl + 'user/logoff',
	
	// 用户注销账号
	userPhoneBindUrl: ApiRootUrl + 'user/phone/bind',
	
    // 扫码登录
    pcLoginUrl: WebRootUrl + 'wx_login',


	//用户信息接口
    userInfoUrl: ApiRootUrl + 'user/info',

	//修改用户信息接口
    updateInfoUrl: ApiRootUrl + 'user/update/info',

	updateInfoByIdUrl: ApiRootUrl + 'user/info/byUserId',

	//用户关注接口
    userFollowUrl: ApiRootUrl + 'user/follow',

	//用户相关笔记接口
    userPostsUrl: ApiRootUrl + 'user/posts',

	//用户相关笔记count接口
    userTotalPostUrl: ApiRootUrl + 'user/totalPost',

	//用户反馈接口
	userBlackUrl: ApiRootUrl + 'user/black',

	//用户反馈接口
	userReportUrl: ApiRootUrl + 'user/report',

	//用户反馈接口
    userFeedbackUrl: ApiRootUrl + 'feedback/add',
	// 用户反馈列表
	getUserFeedbackedUrl: ApiRootUrl + 'feedback/feedbacked',

	// 是否解决
	userFeedbackResolveUrl: ApiRootUrl + 'feedback/resolve',

	//用户认证接口
    userAuthenticationUrl: ApiRootUrl + 'user/authentication',

	postPayUrl: ApiRootUrl + 'posts/pay',

	//用户创建的圈子列表接口
    userCricleUrl: ApiRootUrl + 'user/cricle',

	getNearlyAndFollowCircleUrl: ApiRootUrl + 'circle/nearlyAndfollow',

	//通过用户id获取用户公开信息接口
    getUserinfoByIdUrl: ApiRootUrl + 'user/info/byUserId',

	//通过用户id获取用户笔记接口
    userPostsByIdUrl: ApiRootUrl + 'user/posts/byUserId',

	//关注列表接口
    followUserUrl: ApiRootUrl + 'user/followUser',

	//粉丝列表接口
    fansUserUrl: ApiRootUrl + 'user/fansUser',

	// 获取用户接口
	getUsersUrl: ApiRootUrl + 'user/users',

	// 艾特搜索
	getUserAiteSearchUrl: ApiRootUrl + 'user/aite/search',

	//用户订单列表接口
    myOrderUrl: ApiRootUrl + 'user/myOrder',

    myFinancialUrl: ApiRootUrl + 'user/myFinancial',

	//用户发起提现接口
    initiateWithdrawalUrl: ApiRootUrl + 'user/initiateWithdrawal',


	//用户提现列表接口
    myUserWithdrawalUrl: ApiRootUrl + 'user/myUserWithdrawal',


	//用户余额列表接口
    myUserExceptionalUrl: ApiRootUrl + 'user/myUserExceptional',

	//余额记录
	myUserFinancialRecordUrl: ApiRootUrl + 'user/financialRecord',

	//金币记录
	myUserCoinsRecordUrl: ApiRootUrl + 'user/coinsRecord',

	//余额排行榜
	userEarningsRankUrl: ApiRootUrl + 'user/earnings/rank',

	//免费领取会员接口
    // freeGetVipUrl: ApiRootUrl + 'user/freeGetVip',

    // 圈子
	//轮播图接口
    circlesBannerUrl: ApiRootUrl + 'circles/banner',

	 //分区列表接口
    optionsListUrl: ApiRootUrl + 'posts/plate/options',

   //创建/修改圈子接口
    addCircleUrl: ApiRootUrl + 'posts/add/circle',

	//分区列表接口
    plateListUrl: ApiRootUrl + 'posts/plate/list',

	//视频滑动
	getPostsVideoSlideUrl: ApiRootUrl + 'posts/video/slide',

   //通过分区ID获取圈子接口
    circleByplateidUrl: ApiRootUrl + 'posts/circle/byplateid',

	//搜索圈子接口
    circleSearchUrl: ApiRootUrl + 'posts/circle/search',

	//推荐圈子4接口
    circleRecommendUrl: ApiRootUrl + 'circle/recommend',

	//热门圈子接口
    circleNotUrl: ApiRootUrl + 'circle/hot',

	//全部推荐圈子接口
    circleCircleAndPostsUrl: ApiRootUrl + 'circle/circleAndPosts',

	 //用户关注圈子列表接口
    userFollowCircleListUrl: ApiRootUrl + 'user/follow/CircleList',

   //用户关注圈子接口
    userFollowCircleUrl: ApiRootUrl + 'user/follow/circle',

	//圈子详情接口
    circleInfoUrl: ApiRootUrl + 'circle/info',

	//圈子分享接口
	circleShareUrl: ApiRootUrl + 'app/circle/poster',

	//圈子笔记接口
    postsByCircleIdUrl: ApiRootUrl + 'posts/byCircleId',

	postsByRelevantUrl: ApiRootUrl + 'posts/relevant',

	//审核笔记接口
    userAuditPostsUrl: ApiRootUrl + 'user/auditPosts',

	//圈子关注用户列表接口
    getCircleUserListUrl: ApiRootUrl + 'circle/getCircleUserList',

    //笔记
	 //发帖时推荐的话题列表接口
    tagsRecommendUrl: ApiRootUrl + 'tags/recommend',

   //添加话题接口
    tagsAddUrl: ApiRootUrl + 'tags/add',

	//话题信息
	tagsInfoUrl: ApiRootUrl + 'tags/info',

    //发帖接口
    postAddUrl: ApiRootUrl + 'post/add',

	//发帖喜欢接口
    postsLikeUrl: ApiRootUrl + 'posts/like',

	 //发帖收藏接口
    postsCollectUrl: ApiRootUrl + 'posts/collect',

	//发帖图片不适接口
	postsBlurUrl: ApiRootUrl + 'posts/blur',
	// 转移圈子
	postsMoveUrl: ApiRootUrl + 'posts/move',
	//审核笔记
	postsExamineUrl: ApiRootUrl + 'posts/examine',

	//置顶内容
	postsStickyUrl: ApiRootUrl + 'posts/sticky',

   //删除笔记接口
    postsDeleteUrl: ApiRootUrl + 'posts/delete',

	//笔记详情接口
    postsDetailUrl: ApiRootUrl + 'posts/detail',

	//发表评论接口
    commentAddUrl: ApiRootUrl + 'comment/add',

	//通过笔记id获取评论列表接口
    getCommentByPostsIdUrl: ApiRootUrl + 'comment/byPostsId',

	//通过笔记id获取评论列表接口
	getCommentByCommentIdUrl: ApiRootUrl + 'comment/byCommentId',

	//点赞评论接口
    commentLikeAddUrl: ApiRootUrl + 'comment/like/add',

	//删除评论接口
    commentDeleteAddUrl: ApiRootUrl + 'comment/delete',

	//删除评论接口
	commentStickyAddUrl: ApiRootUrl + 'comment/sticky',

	//充电列表接口
    getExceptionalListUrl: ApiRootUrl + 'posts/getExceptionalList',

    // 首页
	//推荐笔记接口
    indexPostsUrl: ApiRootUrl + 'index/posts',

	//搜索接口
    indexSearchUrl: ApiRootUrl + 'index/search',

	//搜索个数量接口
    searchCountUrl: ApiRootUrl + 'search/count',

	//热门搜索接口
    searchHotListUrl: ApiRootUrl + 'search/hot/list',

	//全部热门话题接口
    tagsHotUrl: ApiRootUrl + 'tags/hot',

	//话题获取笔记列表(瀑布流)接口
    postsTageUrl: ApiRootUrl + 'posts/tags',

	//用户分区列表接口
    userPlateUrl: ApiRootUrl + 'user/plate',

	//用户添加分区接口
    userPlateAddUrl: ApiRootUrl + 'user/plate/add',

	//用户删除分区接口
    userPlateDeleteUrl: ApiRootUrl + 'user/plate/delete',

	//用户投票
	userVoteUrl: ApiRootUrl + 'user/vote',

	//话题获取笔记列表接口
    postsTageV2Url: ApiRootUrl + 'posts/tagsv2',

	//首页轮播搜索关键词列表接口
    searchCarouselListUrl: ApiRootUrl + 'search/carousel/list',

	//用户搜索记录列表接口
    mySearchListUrl: ApiRootUrl + 'search/my/list',

	//用户删除搜索记录接口
    myDelSearchUrl: ApiRootUrl + 'user/myDelSearch',

	//用户删除全部搜索记录接口
    myDelAllSearchUrl: ApiRootUrl + 'user/myDelAllSearch',

    // SHOP
	//banner图
    getShopBannerAndNavUrl: ApiRootUrl + 'shop/bannerAndNav',
	//通知详情
    getShopNoticeDetailUrl: ApiRootUrl + 'shop/noticeDetail',

   //分类
    getShopClassifyUrl: ApiRootUrl + 'shop/classify',

	//首页推荐
    getShopIndexRecommendUrl: ApiRootUrl + 'shop/indexRecommend',

	//商品详情
    getShopGoodsDetailsUrl: ApiRootUrl + 'shop/goodsDetails',

	// 商品海报数据
	getShopGoodsPosterUrl: ApiRootUrl + 'app/shop/goods/poster',

	// 用户海报
	getUserPosterUrl: ApiRootUrl + 'app/user/poster',

	//视频推荐
	getRecommendedVideosUrl: ApiRootUrl + 'posts/video',

	//灵感推荐
    getShopInspirationUrl: ApiRootUrl + 'shop/inspiration',

	//热榜推荐
    getShopHotListUrl: ApiRootUrl + 'shop/hotList',



	 //分区推荐
    getShopPlateUrl: ApiRootUrl + 'shop/plate',

   //分类查询商品
    getShopClassifyGoodsListUrl: ApiRootUrl + 'shop/classifyGoodsList',

	//搜索商品
    getShopGoodsSearchUrl: ApiRootUrl + 'shop/goodsSearch',

	//商品晒单2条
    getShopGoodsPostsUrl: ApiRootUrl + 'shop/goodsPosts',

	//商品晒单列表
    getShopGoodsPostsListUrl: ApiRootUrl + 'shop/goodsPostsList',

	//添加购物袋
    getShopAddCartUrl: ApiRootUrl + 'shop/addCart',

	//用户购物袋列表
    getShopUserCartListUrl: ApiRootUrl + 'shop/userCartList',

	//修改购物袋商品数量
    getShopSaveCartNumUrl: ApiRootUrl + 'shop/saveCartNum',

	//选中购物袋商品
    getShopCheckCartGoodsUrl: ApiRootUrl + 'shop/checkCartGoods',

	//全选购物袋商品
    getShopAllCheckCartGoodsUrl: ApiRootUrl + 'shop/allCheckCartGoods',

	//删除购物袋商品
    getShopDelCartGoodsUrl: ApiRootUrl + 'shop/delCartGoods',

	//用户默认地址
    getShopGetCheckAddsUrl: ApiRootUrl + 'shop/getCheckAdds',

	//用户地址列表
    getShopGetAddsListUrl: ApiRootUrl + 'shop/getAddsList',

	//删除地址
    getShopDelAddsUrl: ApiRootUrl + 'shop/delAdds',

	// 修改订单地址
	shopOrderAddressUrl: ApiRootUrl + 'shop/orderAddress',

	//增加/修改地址
    getShopSaveAddsUrl: ApiRootUrl + 'shop/saveAdds',

	//下单商品列表
    getShopPliceOrderGoodsUrl: ApiRootUrl + 'shop/pliceOrderGoods',


    shopPliceOrderUrl: ApiRootUrl + 'shop/order',

	//app
	//商品下单
    appShopPliceOrderUrl: ApiRootUrl + 'app/shop/order',

	//订单列表
    getShopOrderListUrl: ApiRootUrl + 'shop/getOrderList',

	//取消订单
    getShopCancelOrderUrl: ApiRootUrl + 'shop/cancelOrder',

	//删除订单
    getShopDelOrderUrl: ApiRootUrl + 'shop/delOrder',


	 //重新付款
    shopPaymentUrl: ApiRootUrl + 'shop/payment',

	 //app重新付款
    appShopPaymentUrl: ApiRootUrl + 'app/shop/payment',

   //催发货
    getShopPushDeliveryUrl: ApiRootUrl + 'shop/pushDelivery',

	//确认收货
    getShopConfirmReceiptUrl: ApiRootUrl + 'shop/confirmReceipt',

	//申请退款
    getShopOrderRefundUrl: ApiRootUrl + 'shop/orderRefund',

	//退货退款
    getShopRefundGoodsUrl: ApiRootUrl + 'shop/refundGoods',

	//查看物流
    getShopKuaidiUrl: ApiRootUrl + 'shop/kuaidi',

	//订单列表count
    getShopOrderCountUrl: ApiRootUrl + 'shop/getOrderCount',

	//用户购物袋数量
    userCartCountUrl: ApiRootUrl + 'shop/userCartCount',

    //淘客解析
    shopTaokeParseUrl: ApiRootUrl + 'shop/taoke/parse',

    // 消息
	//消息页数据接口
    getMessagesUrl: ApiRootUrl + 'massages/info',

	//通知详情页数据接口
    getDetailsMessagesUrl: ApiRootUrl + 'massages/getDetailsMessages',

	//已读对应类通知接口
    readMessagesUrl: ApiRootUrl + 'massages/readMessages',

	//发起聊天接口
    addChatUrl: ApiRootUrl + 'massages/addChat',

	//查询用户聊天记录接口
    getUserChatUrl: ApiRootUrl + 'massages/getUserChat',

	//查询用户聊天记录列表接口
    getUserChatListUrl: ApiRootUrl + 'massages/getUserChatList',

	//已读对应用户信息接口
    readUserChatUrl: ApiRootUrl + 'massages/readUserChat',

	//查询用户是否有未读信息
    getSysMessageCountUrl: ApiRootUrl + 'massages/getSysMessageCount',

	//用户删除聊天记录
    userDelMessageUrl: ApiRootUrl + 'massages/userDelMessage',

    //支付
    orderUrl: ApiRootUrl + 'order',
	//POST发起订单接口
    appOrderUrl: ApiRootUrl + 'app/order',

	// 苹果通知url
	appleNotifyUrl: ApiRootUrl + 'app/apple/notify/app',


	//获取开通会员信息接口
    getMembersPriceUrl: ApiRootUrl + 'getMembersPrice',

	// 我的名片
	getUserCodecardUrl: ApiRootUrl + 'user/codecard',

	getUserMembersDataUrl: ApiRootUrl + 'user/membersdata',

	userInviteMember: ApiRootUrl + 'user/invite_member',

	userUploadedUnusedAttachments: ApiRootUrl + 'user/uploaded/unused-attachments',

	// 支付密码
	paycodeSetUrl: ApiRootUrl + 'user/paycode',

    // 公共接口
	// 笔记分享
	getPostsShareUrl: ApiRootUrl + 'posts/share',
	//生成海报
    getPostsMakeShowQcodeUrl: ApiRootUrl + 'posts/makeShowQcode',
	// 笔记反馈
	getPostsFeedbackUrl: ApiRootUrl + 'posts/feedback',
	// 用户不喜欢的笔记
	getPostsUserBlackUrl: ApiRootUrl + 'posts/user/black',
	// 付费推广
	getPostsPpromotionUrl: ApiRootUrl + 'posts/promotion',
    //上传文件接口
    uploadsUrl: ApiRootUrl + 'files/uploads',

	//上传文件接口
	delFileUrl: ApiRootUrl + 'files/del',

    //条款接口
    getClauseDetailUrl: ApiRootUrl + 'common/getClauseDetail',

	//配置数据接口
    configDatalUrl: ApiRootUrl + 'configData',

    //检查支付状态
    checkPayStatus: ApiRootUrl + 'app/pay/status',

	// 获取图集列表
	getWallpaperListUrl: ApiRootUrl + 'wallpaper/list',
	// 图集详情
	getWallpaperDetailUrl: ApiRootUrl + 'wallpaper/detail',
	// 收藏图集
	collectWallpaperUrl: ApiRootUrl  + 'wallpaper/action/collect',
	// 收藏的图集
	getCollectedWallpaperUrl: ApiRootUrl  + 'wallpaper/collected',

	// 获取热门问题列表
	getQuestionsUrl: ApiRootUrl  + 'help/questions',

	getHelpDetailUrl: ApiRootUrl  + 'help/detail',

	getLuckDrawInfoUrl: ApiRootUrl + 'luckdraw/info',

	userLuckDrawUrl: ApiRootUrl + 'user/luckdraw',

	userSignUrl: ApiRootUrl + 'user/sign',

	getSignTaskDataUrl: ApiRootUrl + 'signtask',

	getTaskHallUrl: ApiRootUrl + 'task/hall',

	userTaskReward: ApiRootUrl + 'user/task/complete',

	// 活动列表
	getActivityListUrl: ApiRootUrl  + 'common/activity',

	// 说明列表
	getHelpQuestionsUrl: ApiRootUrl  + 'common/getHelp',

	// 页面模板
	getPageTabsUrl: ApiRootUrl  + 'page/tabs',
	getPageMenusUrl: ApiRootUrl  + 'page/menus',
	getPageQrcodesUrl: ApiRootUrl  + 'page/qrcodes',
	// h5公众号登录轮询
	getIntervalMploginUrl: ApiRootUrl  + 'interval/mplogin',

	getAdminDashboardDataUrl: ApiRootUrl + 'admin/dashboard',
	getAdminUserImgsUrl: ApiRootUrl + 'admin/imgs',
	adminAttachmentDeleteUrl: ApiRootUrl + 'admin/attachment/delete',

	updateAdminSettingUrl: ApiRootUrl + 'admin/setting',

	updateCircleSettingUrl: ApiRootUrl + 'admin/circle/setting',

	getRealtimePostsUrl : ApiRootUrl + 'realtime/top/posts',

	getRealtimeTopicsUrl: ApiRootUrl + 'realtime/top/topics',

	commonBlancePayUrl: ApiRootUrl + 'balance_pay',

	rewardVideogift: ApiRootUrl + 'user/reward_gift',

	getUserInviteInfoUrl: ApiRootUrl + 'user/invite/info',

	getUserInviteListUrl: ApiRootUrl + 'user/invite/list',

	userAvatarframeWearUrl: ApiRootUrl + 'user/avatar_frame/wear',

	putLogUrl: ApiRootUrl + 'common/putlog',

    geocodeUrl: ApiRootUrl + 'geocode/regeo'
};
