const api = require('@/config/api');
const util = require('@/utils/util');

uni.wen = {};
uni.wen.api = api;
uni.wen.util = util;
uni.wen.toUrl = function (type, id, that) {
	that = that || undefined;

	if (type == undefined) {
		uni.showToast({ title: "链接错误", icon: 'none' })
		return;
	} else if (type == -1 || type == '-1') {
		// 返回首页
		uni.reLaunch({
			url: '/pages/tabbar/index/index'
		})
	} else if (type == -2 || type == '-2') {
		// 返回
		uni.navigateBack({
			fail: function () {
				uni.reLaunch({
					url: '/pages/tabbar/index/index'
				})
			}
		})
	} else if (type == -3 || type == '-3') {
		uni.switchTab({
			url: '/pages/tabbar/mine/mine'
		})
	} else if (type == -4 || type == '-4') {
		// 登录
		if (uni.$store.state.token) {

		} else {
			if (uni.$store.state.scene == 14) {
				return;
			}
			if (that) {
				that.myToast({
					closeAll: 1
				});
			}
			if (uni.$store.state.config.user && uni.$store.state.config.user.login_style) {
				// #ifdef APP
				if(uni.$store.state.platform == 'ios' && uni.$store.state.config.app.mode == 'examine'){
					uni.navigateTo({
						url: '/pages/common/phone',
					});
					return false;
				}
				// #endif
				
				if(uni.$store.state.ipad){
					if (uni.$store.state.scene == 76) {
						return;
					}
					uni.navigateTo({
						url: '/pages/common/phone',
					});
					return false;
				}
				
				if(uni.$store.state.config.user.login_style.includes(0) || uni.$store.state.config.user.login_style.includes(1) || uni.$store.state.config.user.login_style.includes(3)){
					uni.navigateTo({
						url: '/pages/common/login',
					});
				}else{
					if (uni.$store.state.scene == 76) {
						return;
					}
					uni.navigateTo({
						url: '/pages/common/phone',
					});
				}
			} else {
				
				// #ifdef APP
				if(uni.$store.state.platform == 'ios'){
					if (uni.$store.state.scene == 76) {
						return;
					}
					uni.navigateTo({
						url: '/pages/common/phone',
					});
					return false;
				}
				// #endif
				
				uni.navigateTo({
					url: '/pages/common/login',
				});
				
			}
			return;
		}
	} else if (type == '0' || type == 0) {
		// 链接类型
		// #ifdef MP
		util.setClipboardData(id, '链接已复制');
		// #endif
		// #ifdef APP
		plus.runtime.openURL(id);
		// #endif
		// #ifdef H5
		window.open(id)
		// #endif
	} else if (type == '1' || type == 1) {
		if (uni.$store.state.config.page.animation && uni.$store.state.config.page.animation.animation_1) {
			uni.navigateTo({
				url: '/pages/sticky/sticky?id=' + id,
				animationType: uni.$store.state.config.page.animation.animation_1,
				animationDuration: 250,
			});
		} else {
			uni.navigateTo({
				url: '/pages/sticky/sticky?id=' + id,
			});
		}
	} else if (type == '2' || type == 2) {
		if (uni.$store.state.config.page.animation && uni.$store.state.config.page.animation.animation_2) {
			uni.navigateTo({
				url: '/pages/circle/list?id=' + id,
				animationType: uni.$store.state.config.page.animation.animation_2,
				animationDuration: 250,
			});
		} else {
			uni.navigateTo({
				url: '/pages/circle/list?id=' + id,
			});
		}

	} else if (type == '3' || type == 3) {
		if (uni.$store.state.config.page.animation && uni.$store.state.config.page.animation.animation_3) {
			uni.navigateTo({
				url: '/pagesA/shop/goods-details/goods-details?id=' + id,
				animationType: uni.$store.state.config.page.animation.animation_3,
				animationDuration: 250,
			});
		} else {
			uni.navigateTo({
				url: '/pagesA/shop/goods-details/goods-details?id=' + id,
			});
		}

	} else if (type == '4' || type == 4) {
		if (uni.$store.state.config.page.animation && uni.$store.state.config.page.animation.animation_4) {
			uni.navigateTo({
				url: '/pages/user/user?id=' + id,
				animationType: uni.$store.state.config.page.animation.animation_4,
				animationDuration: 250,
			});
		} else {
			uni.navigateTo({
				url: '/pages/user/user?id=' + id,
			});
		}

	} else if (type == '5' || type == 5) {
		if (uni.$store.state.config.page.animation && uni.$store.state.config.page.animation.animation_5) {
			uni.navigateTo({
				url: '/pages/tags/tags?id=' + id,
				animationType: uni.$store.state.config.page.animation.animation_5,
				animationDuration: 250,
			});
		} else {
			uni.navigateTo({
				url: '/pages/tags/tags?id=' + id,
			});
		}

	} else if (type == '6' || type == 6) {
		if (uni.$store.state.config.page.animation && uni.$store.state.config.page.animation.animation_6) {
			if (id && id.indexOf('/pages') === 0) {
				uni.navigateTo({
					url: id,
					animationType: uni.$store.state.config.page.animation.animation_6,
					animationDuration: 250,
					fail: function () {
						if (id && id.indexOf('/pages/tabbar/') === 0) {
							id = id.replace('/tabbar/', '/tabbar_clone/')
							uni.navigateTo({
								url: id,
								animationType: uni.$store.state.config.page.animation.animation_6,
								animationDuration: 250,
								fail: function () {
									uni.switchTab({ url: id });
								}
							});
						}else{
							uni.switchTab({ url: id });
						}
					}
				});
			} else {
				uni.showToast({ title: '路径错误', icon: 'none' });
			}
		} else {
			if (id && id.indexOf('/pages') === 0) {
				uni.navigateTo({
					url: id,
					fail: function () {
						if (id && id.indexOf('/pages/tabbar/') === 0) {
							id = id.replace('/tabbar/', '/tabbar_clone/')
							uni.navigateTo({
								url: id,
								fail: function () {
									uni.switchTab({ url: id });
								}
							});
						}else{
							uni.switchTab({ url: id });
						}
					}
				});
			} else {
				uni.showToast({ title: '路径错误', icon: 'none' })
			}
		}

	} else if (type == '7' || type == 7) {
		if (!id || id == '' || id == undefined) {
			id = 0;
		}
		if (uni.$store.state.config.page.animation && uni.$store.state.config.page.animation.animation_7) {
			uni.navigateTo({
				url: '/pagesA/shop/goods-classify/goods-classify?focusShow=&id=' + id,
				animationType: uni.$store.state.config.page.animation.animation_7,
				animationDuration: 250,
			});
		} else {
			uni.navigateTo({
				url: '/pagesA/shop/goods-classify/goods-classify?focusShow=&id=' + id,
			});
		}

	} else if (type == '8' || type == 8) {
		if (uni.$store.state.config.page.animation && uni.$store.state.config.page.animation.animation_8) {
			uni.navigateTo({
				url: '/pages/help/helpDetail/helpDetail?id=' + id,
				animationType: uni.$store.state.config.page.animation.animation_8,
				animationDuration: 250,
			});
		} else {
			uni.navigateTo({
				url: '/pages/help/helpDetail/helpDetail?id=' + id,
			});
		}

	} else if (type == '9' || type == 9) {
		util.scanCodeMethods();
	} else if (type == '10' || type == 10) {
		if (uni.$store.state.config.page.animation && uni.$store.state.config.page.animation.animation_10) {
			uni.navigateTo({
				url: '/pages/web-view/index?url=' + encodeURIComponent(id),
				animationType: uni.$store.state.config.page.animation.animation_10,
				animationDuration: 250,
			});
		} else {
			uni.navigateTo({
				url: '/pages/web-view/index?url=' + encodeURIComponent(id),
			});
		}

	} else if (type == '11' || type == 11) {
		let id_arr = id.split('|');
		if (id_arr.length = 3) {
			util.toMpWeixin(id_arr[0], id_arr[1], id_arr[2]);
		} else {
			util.toMpWeixin(id_arr[0], id_arr[1], '');
		}
	} else if (type == '12' || type == 12) {
		if (uni.$store.state.config.page.animation && uni.$store.state.config.page.animation.animation_12) {
			uni.navigateTo({
				url: '/pagesA/mine/qrcode/qrcode?user_id=' + id,
				animationType: uni.$store.state.config.page.animation.animation_12,
				animationDuration: 250,
			})
		} else {
			uni.navigateTo({
				url: '/pagesA/mine/qrcode/qrcode?user_id=' + id,
			})
		}
	} else if (type == '13' || type == 13) {
		if (id) {
			var left = id.indexOf('(');
			var right = id.indexOf(')');
			var para_length = right - left - 1;
			var function_name = id.substr(0, left).trim();
			var para_str = id.substr(left + 1, para_length);
			if (para_str.indexOf(',') === -1) {
				if (function_name == 'flushPlateContent' && that) {
					that.flushPlateContent(para_str);
				}
			} else {
				var para_arr = para_str.split(',');
				var new_para_arr = [];
				for (var p in para_arr) {
					new_para_arr.push(para_arr[p].trim());
				}
				// 暂时没有相关函数
			}
		} else {
			uni.showToast({
				title: '缺少参数',
				icon: 'none'
			});
		}
	} else if (type == '14' || type == 14) {
		// 视频号主页
		if (id) {
			wx.openChannelsUserProfile({
				finderUserName: id
			});
		}

	} else if (type == '15' || type == 15) {
		if (id) {
			// 视频号-视频
			let id_arr = id.split('|');
			wx.openChannelsActivity({
				finderUserName: id_arr[0],
				feedId: id_arr[1]
			});
		}

	} else if (type == '16' || type == 16) {
		// 视频号-直播
		if (id) {
			wx.openChannelsLive({
				finderUserName: id
			});
		}
	} else if (type == '17' || type == 17) {
		util.setClipboardData(id, '内容已复制');
	} else if (type == '18' || type == 18) {
		if (uni.$store.state.config.page.animation && uni.$store.state.config.page.animation.animation_18) {
			uni.navigateTo({
				url: '/pages/message/detail/detail?userid=' + id,
				animationType: uni.$store.state.config.page.animation.animation_18,
				animationDuration: 250,
			});
		} else {
			uni.navigateTo({
				url: '/pages/message/detail/detail?userid=' + id,
			});
		}

	} else if (type == '19' || type == 19) {
		if (id) {
			// 视频号-视频
			let id_arr = id.split('|');
			wx.openEmbeddedMiniProgram({
				appId: id_arr[0],
				path: id_arr[1]
			});
		}
	} else if (type == '20' || type == 20) {
		if (uni.$store.state.config.page.animation && uni.$store.state.config.page.animation.animation_20) {
			uni.navigateTo({
				url: '/pages/wallpaper/detail?id=' + id,
				animationType: uni.$store.state.config.page.animation.animation_20,
				animationDuration: 250,
			});
		} else {
			uni.navigateTo({
				url: '/pages/wallpaper/detail?id=' + id,
			});
		}
	} else if (type == '21' || type == 21) {
		if (uni.$store.state.config.page.animation && uni.$store.state.config.page.animation.animation_21) {
			uni.navigateTo({
				url: '/pages/wallpaper/list?id=' + id,
				animationType: uni.$store.state.config.page.animation.animation_21,
				animationDuration: 250,
			});
		} else {
			uni.navigateTo({
				url: '/pages/wallpaper/list?id=' + id,
			});
		}
	} else if (type == '22' || type == 22) {
		uni.makePhoneCall({
			phoneNumber: id,
			success: function () {

			},
			fail: function (error) {

			}
		});
	} else if (type == '23' || type == 23) {
		// uni.openEmail({
		// 	to: [emailAddress],
		// 	subject: '邮件主题',
		// 	body: '邮件正文',
		// 	cc: [], // 抄送人邮箱地址列表，可省略
		// 	bcc: [] // 密送人邮箱地址列表，可省略
		// });
	} else if (type == '24' || type == 24) {
		if (uni.$store.state.$vm) {
			uni.$store.state.$vm.myToast({
				type: 'popImg',
				timeout: 2000,
				img: id,
				isClick: true,
				mask: 1,
			});
			return false;
		} else {
			let pages = getCurrentPages();
			//这句话 获取的才是当前页面实例
			let currentPage = pages[pages.length - 1];
			if (currentPage == undefined) {
				return;
			}
			// #ifdef MP
			currentPage = currentPage.$vm;
			// #endif
			currentPage.myToast({
				type: 'popImg',
				timeout: 2000,
				img: id,
				isClick: true,
				mask: 1,
			});
			return false;
		}
	} else if (type == '25' || type == 25) {
		uni.showToast({
			title: id,
			icon: 'none'
		})
	}
};


uni.miniLoading = function (flag) {
	if (flag) {
		try {
			if (uni.$store.state.$vm) {
				uni.$store.state.$vm.myToast({
					type: 'miniloading',
					timeout: 2000,
					isClick: true,
					mask: 3,
					uuid: 'miniloading-xxxx'
				});
				return false;
			} else {
				let pages = getCurrentPages();
				//这句话 获取的才是当前页面实例
				let currentPage = pages[pages.length - 1];
				if (currentPage == undefined) {
					return;
				}
				// #ifdef MP
				currentPage = currentPage.$vm;
				// #endif
				currentPage.myToast({
					type: 'miniloading',
					timeout: 2000,
					isClick: true,
					mask: 3,
					uuid: 'miniloading-xxxx'
				});
				return false;
			}

		} catch (err) {
			uni.showLoading();
		}
	} else {
		try {
			if (uni.$store.state.$vm) {
				uni.$store.state.$vm.myToast({
					close: 1,
					uuid: 'miniloading-xxxx',
				});
				return false;
			} else {
				let pages = getCurrentPages();
				//这句话 获取的才是当前页面实例
				let currentPage = pages[pages.length - 1];
				if (currentPage == undefined) {
					return;
				}
				// #ifdef MP
				currentPage = currentPage.$vm;
				// #endif
				currentPage.myToast({
					close: 1,
					uuid: 'miniloading-xxxx',
				});
				return false;
			}

		} catch (err) {
			uni.hideLoading();
		}


	}

};

uni.wen.px2rpx = function (num) {
	return (num * 750) / uni.$store.state.windowWidth;
}

// #ifdef MP-WEIXIN
wx.tabBarSetData = function(obj){
	let pages = getCurrentPages();
	//这句话 获取的才是当前页面实例
	let currentPage = pages[pages.length - 1];
	if (currentPage == undefined) {
		return;
	}
	currentPage = currentPage.$vm;
	currentPage.tabBarSetData(obj);
}
// #endif