export default{
	data() {
		return {
			pagebgcolorClass: 'pagebg_white',
			fph: false,
			mytoast_data: {},
			pageTheme: 'default',
		}
	},
	computed:{
		global__theme__: {
			get(){
				let that = this;
				return that.$store.state.theme;
			},
			set(v){}
		},
		global__tenant__: {
			get(){
				let that = this;
				if(that.$store.state.tenant){
					return that.$store.state.tenant;
				}
				
				return {tenant_id: null, name: '', short: ''};
			},
			set(v){}
		},
		isOfficial:{
			get(){
				let that = this;
				if(that.$store.state.config){
					if(that.$store.state.config.admin && that.$store.state.config.admin.playlist){
						if(that.$store.state.config.admin.playlist.includes(that.$store.state.userInfo.id)){
							return 2;
						}
					}
					if(that.$store.state.userInfo.is_official > 0){
						return that.$store.state.userInfo.is_official;
					}
				}
				
				return 0;
			},
			set(v){}
		},
		config_img:{
			get(){
				let that = this;
				if(that.$store.state.config && that.$store.state.config.img){
					return that.$store.state.config.img || {};
				}
				return {
					empty: "",
					default: "",
					default_avatar: ""
				};
			},
			set(v){}
		},
		config_pays:{
			get(){
				let that = this;
				if(that.$store.state.config && that.$store.state.config.pays){
					return that.$store.state.config.pays || {};
				}
				return {};
			},
			set(v){}
		},
		myUserInfo:{
			get(){
				let that = this;
				return that.$store.state.userInfo;
			},
			set(v){}
		},
		primaryColor: {
			get(){
				let that = this;
				if(that.$store.state.config){
					if(that.$store.state.config.color && that.$store.state.config.color.length > 0){
						return that.$store.state.config.color['--color-primary'];
					}
				}
				return '#fc3a72';
			},
			set(v){}
		},
		global__platform__: {
			get(){
				let that = this;
				if(that.$store.state.platform){
					return that.$store.state.platform;
				}
				return 'android';
			},
			set(v){}
		},
		global__device__: {
			get(){
				let that = this;
				if(that.$store.state.device){
					return that.$store.state.device;
				}
				return 'mp';
			},
			set(v){}
		}
	},
	methods: {
		stopPrevent(){return false;},
		miniMoveHandle(){},
		toSliderUrl(type, id){
			let that = this;
			uni.wen.toUrl(type, id, that);
		},
		tabBarSetData(obj) {
			// #ifdef MP-WEIXIN
			if (typeof this.$mp.page.getTabBar === 'function' && this.$mp.page.getTabBar()) {
				this.$mp.page.getTabBar().setData(obj)
			}
			// #endif
		},
		myToast(data){
			if(data){
				data.random = Math.random();
				this.setData({
					mytoast_data: data
				})
			}
		},
		updateScene(scene, scene_id, login = false, admin = false, paycode = 0, config = 0){
			let that = this;
			that.$store.commit('updateScene', {scene: scene, scene_id: scene_id});
			
				if(that.pageTheme != that.$store.state.theme){
					if(that.$store.state.theme == 'black'){
						if([1,2,4,5,19,50001,50].includes(scene)){
							// #ifdef MP-WEIXIN
							that.tabBarSetData({
								color: "rgb(107,111,119)",
								selectColor: "rgb(255,255,255)",
								background: "#19191e;",
								popupStyle: {
									bgcolor: "#19191e",
									itemBgcolor: "#26262b",
									fontColor: "#ffffff"
								}
							});
							// #endif
							// #ifndef MP-WEIXIN
							uni.setTabBarStyle({
								color: "#6b6f77",
								selectedColor: "#ffffff",
								backgroundColor: "#19191e",
								borderStyle: "#19191e",
							});
							let index = 0;
							if(scene == 2 || scene == 19){
								index = 1;
							}else if(scene == 4){
								index = 2;
							}else if(scene == 5){
								index = 3;
							}
							uni.setTabBarItem({
								index: index,
								iconfont: {
									color: "#6b6f77",
									selectedColor: "#ffffff"
								},
								fail() {
									setTimeout(function(){
										uni.setTabBarItem({
											index: index,
											iconfont: {
												color: "#6b6f77",
												selectedColor: "#ffffff"
											},
										})
									}, 100);
								}
							})
							// #endif
						}
						
						uni.setNavigationBarColor({
							frontColor: "#ffffff",
							backgroundColor: "#19191e",
							fail(err) {
								console.log(err)
							}
						});
						that.pageTheme = 'black';
					}else{
						if([1,2,4,5,19,50001,50].includes(scene)){
							// #ifdef MP-WEIXIN
							that.tabBarSetData({
								color: "rgb(140,140,140)",
								selectColor: "rgb(50,50,50)",
								background: "#ffffff;",
								popupStyle: {
									bgcolor: "#ffffff",
									itemBgcolor: "#f5f5f5",
									fontColor: "#000000"
								}
							});
							// #endif
							// #ifndef MP-WEIXIN
							uni.setTabBarStyle({
								color: "#8C8C8C",
								selectedColor: "#323232",
								backgroundColor: "#ffffff",
								borderStyle: "#ffffff",
							});
							let index = 0;
							if(scene == 2 || scene == 19){
								index = 1;
							}else if(scene == 4){
								index = 2;
							}else if(scene == 5){
								index = 3;
							}
							uni.setTabBarItem({
								index: scene,
								iconfont: {
									color: "#8C8C8C",
									selectedColor: "#323232"
								},
								fail() {
									setTimeout(function(){
										uni.setTabBarItem({
											index: index,
											iconfont: {
												color: "#8C8C8C",
												selectedColor: "#323232"
											},
										})
									}, 100);
								}
							})
							// #endif
						}
						uni.setNavigationBarColor({
							frontColor: "#000000",
							backgroundColor: "#ffffff",
							fail(err) {
								console.log(err)
							}
						});
						that.pageTheme = 'default';
					}
				}
			
			if(typeof config === 'number'){
				if(config > 0){
					if(config == 2 && that.$store.state.config2.OK == false){
						that.$store.dispatch('initConfig2');
					}
					if(config == 3 && that.$store.state.config3.OK == false){
						that.$store.dispatch('initConfig3');
					}
					if(config == 4 && that.$store.state.config4.OK == false){
						that.$store.dispatch('initConfig4');
					}
				}
			}else if(Array.isArray(config) ){
				config.forEach(function(item){
					if(item == 2 && that.$store.state.config2.OK == false){
						that.$store.dispatch('initConfig2');
					}
					if(item == 3 && that.$store.state.config3.OK == false){
						that.$store.dispatch('initConfig3');
					}
					if(item == 4 && that.$store.state.config4.OK == false){
						that.$store.dispatch('initConfig4');
					}
				})
			}
			
			if(scene != 14 && scene != 52 && scene != 37){
				if(login || (that.$store.state.config.user && that.$store.state.config.user.login_force == 1)){
					if (uni.$store.state.token) {
						
					}else{
						if(that.$store.state.scene == 14){
							return;
						}
						that.myToast({
							closeAll: 1
						});
						uni.wen.toUrl(-4, null, null);
						return;
					}
				}
				if(admin && (that.$store.state.userInfo.is_official == 0 || that.$store.state.userInfo.is_official === false) && that.$store.state.userInfo.is_played == false){
					uni.showToast({
						title: '仅允许管理员访问',
						icon: 'none'
					});
					setTimeout(function(){
						uni.wen.toUrl(-2, 0, null);
					}, 1500);
					return;
				}

				if(paycode > 0 && that.$store.state.config.pays['paycode_'+(paycode.toString())] !== true){
					uni.showToast({
						title: 'you are not unlocked this',
						icon: 'none'
					});
					setTimeout(function(){
						uni.wen.toUrl(-2, 0, null);
					}, 1500);
					return;
				}
				
				// #ifdef H5
				if(that.$store.state.config.h5.redirect && that.$store.state.h5_browser != 'wxclient'){
					if(that.$store.state.scene_total >= that.$store.state.config.h5.redirect_pages){
						that.myToast({
							closeAll: 1
						});
						uni.wen.toUrl(6, '/pagesC/redirect/redirect', null);
						return;
					}
				}
				// #endif
				
				if(that.$store.state.config.app.phone.pages.includes(scene)){
					if (uni.$store.state.token) {
						if(that.$store.state.userInfo.phone && that.$store.state.userInfo.phone.length > 5){
							that.myToast({
								type:'getPhone',
								close: 1,
								timeout: 2000,
								isClick:true,
							});
						}else{
							// #ifdef MP
							setTimeout(function(){
								that.myToast({
									type:'getPhone',
									content: '绑定手机号',
									timeout: 2000,
									isClick:true,
								});
							}, 2000);
							// #endif
						}
					}
				}
			}
			
			// 注意：mini js代码插入点001号，请勿删除下面一行
			//script(<<<JS<<<001<<<JS);
			if(scene != 83 && uni.wen.api.homeUrl == 'https://mini.chongyeapp.com'){
			    if(that.global__tenant__.tenant_id === null){
			        uni.wen.toUrl(6, '/pages/common/tenants');
			        return false;
			    }
			}
			
			
			if(that.$store.state.isRefresh == true){
				that.$store.commit('forceUpdatePage', false);
				uni.startPullDownRefresh();
			}
		},
		globalOnloadProcess(options){
			let that = this;
			that.$store.commit('vm', that);
			
			if(options.from_user && options.from_user != null && options.from_user != 'undefined'){
				if((typeof(options.from_user)=='string' && options.from_user.length > 0) || (typeof(options.from_user)=='number' && options.from_user > 0)){
					uni.setStorageSync('form_user', options.from_user);
					uni.$store.commit('updateFormUser', options.from_user);
				}
			}
			
			if(options.fph == 1){
				that.fph = true;
			}
			
			if(options.tenant && options.tenant > 0 && that.$store.state.tenant.tenant_id != options.tenant){
				
				if(that.$store.state.config.app && that.$store.state.config.app.tenants){
					let tenants = that.$store.state.config.app.tenants;
					tenants.forEach((ele) => {
						if(ele.tenant_id == options.tenant){
							uni.setStorageSync('tenant', ele);
							uni.$store.commit('updateTenant', ele);
							
							uni.$store.dispatch('initSystem');
							uni.$store.dispatch('updateConfig');
						}
					})
				}
			}
			
			if(that.pageTheme == 'default'){
				uni.setNavigationBarColor({
					frontColor: "#000000",
					backgroundColor: "#ffffff",
					fail(err) {
						console.log(err)
					}
				});
			}
			
		}
	}
}