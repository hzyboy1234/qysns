export default {  
	setGlobalConfig(state, data) {
		state.OK = true;
		state.page = data.page;
		state.emoji = data.emoji;
		state.invite = data.invite;
		state.app = data.app;
		state.placeholder = data.placeholder;
		state.user = data.user;
	}
}