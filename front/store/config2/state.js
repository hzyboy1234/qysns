export default {
	"OK": false,
	"page": {
		"sticky": {
			"statement": "",
			"slider_comment_page": -1,
			"is_recommendation": 0
		},
		"circle": {
			"publish_icon": 0
		},
		"wallpapers": {
			"name": ""
		},
		"mine": {
			"about": {
				"list": []
			}
		}
	},
	"emoji": {
		"page_1": [],
		"page_2": [],
		"page_3": [],
	},
	"invite": {
		"status": false,
		"lv": 2,
		"max_layer": 3
	},
	"app": {
		"withdrawal": {
			"platform_percent": 10
		}
	},
	"placeholder": {
		"publish_word": ""
	},
	"user": {
		"labels": []
	}
};